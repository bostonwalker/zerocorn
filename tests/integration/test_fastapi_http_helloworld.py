from fastapi import FastAPI
from fastapi.responses import Response, PlainTextResponse
import requezts

import zerocorn


NET_ID = 0x1d719394041eeb5f
HOST = "0.0.0.0"
PORT = 8000


def test_fastapi_http_helloworld():
    """Set up HTTP Hello, World! server using FastAPI"""

    app = FastAPI()

    @app.get("/")
    def _helloworld() -> Response:
        return PlainTextResponse("Hello, World!")

    # noinspection PyTypeChecker
    ip_addr = zerocorn.run(app, net_id=NET_ID, host=HOST, port=PORT)

    with requezts.Session(NET_ID) as session:
        response = session.get(f"http://{ip_addr}:{PORT}/")
        assert response.text == "Hello, World!"
