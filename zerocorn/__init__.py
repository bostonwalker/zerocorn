from typing import Optional, Tuple
from asgiref.typing import ASGI3Application


def run(app: ASGI3Application, net_id: int, host: Optional[str] = None, port: int = 8000) -> Tuple[str, int]:
    """Run application on ZeroTier network
    
    :param app: ASGI 3 application Callable
    :param net_id: ZeroTier network ID (128-bit unsigned int)
    :param host: Host to bind to (default: "0.0.0.0")
    :param port: Port to bind to (default: 8000)
    :return: IP address and port bound to
    :rtype: Tuple[str, int]
    """

    host = host or "0.0.0.0"

    raise NotImplementedError

    return (host, port)
