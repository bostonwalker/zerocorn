# zerocorn

[ASGI](https://asgi.readthedocs.io/en/latest/) protocol server over encrypted P2P sockets using [ZeroTier](https://www.zerotier.com/)
