.. role:: gold

:gold:`z e r o` c o r n
====================================

`ASGI <https://asgi.readthedocs.io/en/latest/>`_ protocol server over encrypted P2P sockets using `ZeroTier <https://www.zerotier.com/>`_.
